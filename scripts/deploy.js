async function main() {
    const REWARD = await ethers.getContractFactory("REWARD")
  
    // Start deployment, returning a promise that resolves to a contract object
    const REWARD = await REWARD.deploy()
    console.log("Contract deployed to address:", REWARD.address)
  }
  
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error)
      process.exit(1)
    })
  