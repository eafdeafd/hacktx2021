pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

/* INVARIANT: REWARDPASSES ARE INITIALIZED WITH ONE REWARD ALWAYS */
contract RewardPass is ERC721URIStorage, Ownable {
    // counter for unique reward passes
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    // keep track of users and passes
    mapping(address => uint256[]) public rewardsOfUser;

    constructor() public ERC721("REWARD_PASS", "RWD_PASS") {}

    function mintRewardPassNFT(address recipient, string memory tokenURI, uint256 initialRewardId) public returns (uint256) {   
        // increment the amount of this token
        _tokenIds.increment();

        // get the new token id for the next token
        uint256 newItemId = _tokenIds.current();

        // mint to the address that will own the new token, send the id of the token to be minted 
        _mint(recipient, newItemId);

        // set the token URI for the minted nft
        _setTokenURI(newItemId, tokenURI);

        // add the new user to the userToRewardPass
        rewardsOfUser[recipient].push(initialRewardId);

        // return the id of the new nft
        return newItemId;
    }

    // add a reward to the battlepass referenced by idOfPass
    function addReward(address user, uint256 idOfReward) public returns(uint256) {
        // error check, rewards of pass must == 1 otherwise the user does not exist
        require(rewardsOfUser[user].length != 0);

        // add to rewardsOfUser
        rewardsOfUser[user].push(idOfReward);

        // return the index of where to find the reward (should be stored off-chain)
        return rewardsOfUser[user].length - 1;
    }

    // consumes a reward in the referenced battlepass at the specific index
    function consumeReward(address user, uint256 idxOfReward) public returns(uint256) {
        // check if user exists and the reward has not already been consumed
        require(rewardsOfUser[user][idxOfReward] != 0);

        // get the id of the reward for the certain user
        uint256 id = rewardsOfUser[user][idxOfReward];

        // burn the nft
        _burn(id);

        // consume the reward
        rewardsOfUser[user][idxOfReward] = 0;

        // return the id that was just burned
        return id;
    }

    function getRewards(address user) public view returns(uint256[] memory) {
        // check if the user exists
        require(rewardsOfUser[user].length != 0);

        // return the array of rewards
        return rewardsOfUser[user];
    }
}
